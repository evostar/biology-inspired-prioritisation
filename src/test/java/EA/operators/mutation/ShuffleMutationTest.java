package EA.operators.mutation;

import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ShuffleMutationTest {

    ShuffleMutation shuffleMutation;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        shuffleMutation = new ShuffleMutation();
    }

    @Test
    void checkMutateContainsSameElementsInDifferentOrder() {
        List<Integer> signals = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<Integer> mutatedSignals = shuffleMutation.mutate(new ArrayList<>(signals));
        Assertions.assertNotEquals(signals, mutatedSignals);
        for(Integer signal : signals){
            Assertions.assertTrue(mutatedSignals.contains(signal));
        }
    }
}