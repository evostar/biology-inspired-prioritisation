package EA.operators.replacement;

import DTO.Fitness;
import common.data.Chromosome;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;

class OldestChildReplacementTest {

    OldestChildReplacement oldestChildReplacement;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        oldestChildReplacement = new OldestChildReplacement();
    }

    @Test
    void replaceWithYoungerChild() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 10);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 10);
        Chromosome chromosomeC = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 11);

        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB, chromosomeC));
        Chromosome child = new Chromosome("D", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 12);
        ConcurrentLinkedQueue<Chromosome> newChromosomes = oldestChildReplacement.replace(chromosomes, child);
        Assertions.assertTrue(newChromosomes.contains(chromosomeB));
        Assertions.assertTrue(newChromosomes.contains(chromosomeC));
        Assertions.assertTrue(newChromosomes.contains(child));
    }

    @Test
    void getOldest() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 1);
        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB));
        Assertions.assertEquals(oldestChildReplacement.getOldest(chromosomes), chromosomeA);
    }
}