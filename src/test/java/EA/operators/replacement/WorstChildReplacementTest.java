package EA.operators.replacement;

import DTO.Fitness;
import common.data.Chromosome;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;

class WorstChildReplacementTest {

    WorstChildReplacement worstChildReplacement;

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @BeforeEach
    void setUp() {
        worstChildReplacement = new WorstChildReplacement();
    }

    @Test
    void replaceWithBetterChild() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);

        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB));        Chromosome child = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        ConcurrentLinkedQueue<Chromosome> newChromosomes = worstChildReplacement.replace(chromosomes, child);
        Assertions.assertEquals(newChromosomes.size(), 2);
        Assertions.assertTrue(newChromosomes.contains(chromosomeA));
        Assertions.assertTrue(newChromosomes.contains(child));
    }

    @Test
    void replaceWithEqualChild() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);

        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB));        Chromosome child = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);
        ConcurrentLinkedQueue<Chromosome> newChromosomes = worstChildReplacement.replace(chromosomes, child);
        Assertions.assertEquals(newChromosomes.size(), 2);
        Assertions.assertTrue(newChromosomes.contains(chromosomeA));
        Assertions.assertTrue(newChromosomes.contains(chromosomeB));
    }

    @Test
    void replaceWithWorseChild() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);

        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB));        Chromosome child = new Chromosome("C", new ArrayList<>(), new ArrayList<>(), new Fitness(3.0, 0.0, 0.0, 0.0), 0);
        ConcurrentLinkedQueue<Chromosome> newChromosomes = worstChildReplacement.replace(chromosomes, child);
        Assertions.assertEquals(newChromosomes.size(), 2);
        Assertions.assertTrue(newChromosomes.contains(chromosomeA));
        Assertions.assertTrue(newChromosomes.contains(chromosomeB));
    }

    @Test
    void getWorst() {
        Chromosome chromosomeA = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), new Fitness(1.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", new ArrayList<>(), new ArrayList<>(), new Fitness(2.0, 0.0, 0.0, 0.0), 0);
        ConcurrentLinkedQueue chromosomes = new ConcurrentLinkedQueue(Arrays.asList(chromosomeA, chromosomeB));
        Assertions.assertEquals(worstChildReplacement.getWorst(chromosomes), chromosomeB);
    }
}