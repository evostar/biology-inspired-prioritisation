package EA.data;

import DTO.Fitness;
import common.data.Chromosome;
import common.service.ParallelisationService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class ChromosomeTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
        ParallelisationService.generateIds(4);
    }

    @Test
    void checkNewChromosomeId(){
        String id = ParallelisationService.uids.peek();
        int ids = ParallelisationService.uids.size();
        Chromosome chromosome = new Chromosome(false);
        Assertions.assertEquals(id, chromosome.getId());
        Assertions.assertEquals(ParallelisationService.uids.size(), ids - 1);
    }

    @Test
    void getFitness() {
        Fitness fitness = new Fitness(1.0, 2.0, 3.0, 4.0);
        Chromosome chromosome = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), fitness, 0);
        Assertions.assertEquals(chromosome.getFitness(), fitness);
    }

    @Test
    void getFitnessNull() {
        Fitness nullFitness = new Fitness(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        Chromosome chromosome = new Chromosome("A", new ArrayList<>(), new ArrayList<>(), null, 0);
        Assertions.assertEquals(chromosome.getFitness().getValue(), nullFitness.getValue());
        Assertions.assertEquals(chromosome.getFitness().getTimeForTargetVehicle(), nullFitness.getTimeForTargetVehicle());
        Assertions.assertEquals(chromosome.getFitness().getTimeForAllVehicles(), nullFitness.getTimeForAllVehicles());
    }

    @Test
    void initialiseRandom() {
        Chromosome chromosome = new Chromosome(true);
        Assertions.assertTrue(chromosome.getDurations().size() >= Configuration.rc.minPhases());
        Assertions.assertTrue(chromosome.getDurations().size() <= Configuration.rc.maxPhases());
        Assertions.assertTrue(chromosome.getSignals().size() == chromosome.getDurations().size());
        chromosome.getDurations().forEach(duration -> {
            Assertions.assertTrue(duration >= Configuration.rc.minPhaseDuration());
            Assertions.assertTrue(duration <= Configuration.rc.maxPhaseDuration());
        });
        Integer prevState = -1;
        for (Integer state : chromosome.getSignals()) {
            Assertions.assertTrue(state >= 0);
            Assertions.assertTrue(state < Configuration.sc.states().size());
            if(prevState > -1){
                Assertions.assertFalse(prevState.equals(state));
            }
            prevState = state;
        }
    }
}