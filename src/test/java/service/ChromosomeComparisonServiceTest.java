package service;

import DTO.Fitness;
import common.data.Chromosome;
import common.service.ChromosomeComparisonService;
import configuration.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class ChromosomeComparisonServiceTest {

    @BeforeAll
    static void setUpAll(){
        Configuration configuration = new Configuration("");
    }

    @Test
    void simpleDistance() {
        List<Integer> durationsA = Arrays.asList(5, 5, 5);
        List<Integer> durationsB = Arrays.asList(5, 10, 5);
        double distance = ChromosomeComparisonService.calculateEuclideanDistanceForList(durationsA, durationsB);
        Assertions.assertEquals(distance, 5);
    }

    @Test
    void mediumDistance() {
        List<Integer> durationsA = Arrays.asList(0, 5, 5);
        List<Integer> durationsB = Arrays.asList(5, 10, 3);
        double distance = ChromosomeComparisonService.calculateEuclideanDistanceForList(durationsA, durationsB);
        Assertions.assertEquals(distance, Math.sqrt(54));
    }

    @Test
    void complexDistanceA() {
        List<Integer> durationsA = Arrays.asList(0, 5, 5);
        List<Integer> durationsB = Arrays.asList(5, 10, 3, 6, 10);
        double distance = ChromosomeComparisonService.calculateEuclideanDistanceForList(durationsA, durationsB);
        Assertions.assertEquals(distance, Math.sqrt(224));
    }

    @Test
    void complexDistanceB() {
        List<Integer> durationsA = Arrays.asList(0, 5, 5, 1);
        List<Integer> durationsB = Arrays.asList(0, 5, 5);
        double distance = ChromosomeComparisonService.calculateEuclideanDistanceForList(durationsA, durationsB);
        Assertions.assertEquals(distance, 2);
    }

    @Test
    void simpleCombinedDistance() {
        Chromosome chromosomeA = new Chromosome("A", Arrays.asList(5, 10), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", Arrays.asList(10, 10), Arrays.asList(1, 4), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        double distance = ChromosomeComparisonService.calculateEuclideanDistance(chromosomeA, chromosomeB);
        Assertions.assertEquals(distance, 7);
    }

    @Test
    void complexCombinedDistance() {
        Chromosome chromosomeA = new Chromosome("A", Arrays.asList(5, 10), Arrays.asList(1, 2), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        Chromosome chromosomeB = new Chromosome("B", Arrays.asList(10, 10, 5), Arrays.asList(1, 4, 3), new Fitness(0.0, 0.0, 0.0, 0.0), 0);
        double distance = ChromosomeComparisonService.calculateEuclideanDistance(chromosomeA, chromosomeB);
        Assertions.assertEquals(distance, Math.sqrt(61) + Math.sqrt(20));
    }


}