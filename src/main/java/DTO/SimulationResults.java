package DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Wrapper for the results of a single simulation
 */
@NoArgsConstructor
@AllArgsConstructor
public class SimulationResults {

    private static final Logger logger = LogManager.getLogger(SimulationResults.class.getName());

    private Double timeForTargetVehicle;
    private Double timeForAllVehicles;
    @Getter
    @Setter
    private List<Penalty> penalties;
    @Getter
    private Boolean wasAborted;

    // Safety measures in case of null pointers
    public Double getTimeForTargetVehicle() {
        if (timeForTargetVehicle == null) {
            logger.error("Tried to access time for target vehicle, but it is null, returning worst value instead.");
            return Double.MAX_VALUE;
        }
        return timeForTargetVehicle;
    }

    // Safety measures in case of null pointers
    public Double getTimeForAllVehicles() {
        if (timeForAllVehicles == null) {
            logger.error("Tried to access time for all vehicles, but it is null, returning worst value instead.");
            return Double.MAX_VALUE;
        }
        return timeForAllVehicles;
    }
}
