package DTO;

import lombok.Getter;
import lombok.Setter;

/**
 * Wrapper for the fitness / the evaluation result of a single chromosome
 * Fitness can either be normal or shared
 */
@Getter
public class Fitness {

    enum Type{
        NORMAL, SHARED;
    }

    private Type type;

    // The actual fitness
    private Double value;
    // The shared fitness
    private Double shared;

    private Double timeForTargetVehicle;
    private Double timeForAllVehicles;
    private Double totalPenalty;
    @Setter
    private Boolean wasAborted = false; // If the simulation was aborted, most likely because the time ran out

    public Fitness(Double value, Double timeForTargetVehicle, Double timeForAllVehicles, Double totalPenalty) {
        this.value = value;
        this.type = Type.NORMAL;
        this.timeForTargetVehicle = timeForTargetVehicle;
        this.timeForAllVehicles = timeForAllVehicles;
        this.totalPenalty = totalPenalty;
    }

    public Fitness(Double value, Double timeForTargetVehicle, Double timeForAllVehicles, Double totalPenalty, Boolean wasAborted) {
        this.value = value;
        this.type = Type.NORMAL;
        this.timeForTargetVehicle = timeForTargetVehicle;
        this.timeForAllVehicles = timeForAllVehicles;
        this.totalPenalty = totalPenalty;
        this.wasAborted = wasAborted;
    }

    public void setSharedFitness(double shared){
        this.type = Type.SHARED;
        this.shared = shared;
    }

    // Returns normal or shared fitness
    public Double getValue() {
        if(type.equals(Type.SHARED)){
            return shared;
        }
        return value;
    }

    public Double getValueRegardlessOfSharedFitness() {
        return value;
    }
}
