package common.service;

import configuration.Configuration;

import java.util.List;

// This service is used to ensure only valid chromosomes are part of the population
public class RepairService {

    /**
     * Repairs durations by
     * -> shortening the array or
     * -> restricting individual values to to the configured min or max values
     * @param durations the durations to repair
     * @return the repaired duration array
     */
    public static List<Integer> repairDurations(List<Integer> durations) {
        List<Integer> repairedDurations = durations;
        // Shorten if needed
        if(durations.size() > Configuration.rc.maxPhases()){
            repairedDurations = durations.subList(0, Configuration.rc.maxPhases());
        }
        //Set min and max, if needed
        for(int i = 0; i < durations.size(); i++){
            if(durations.get(i) < Configuration.rc.minPhaseDuration()){
                durations.set(i, Configuration.rc.minPhaseDuration());
            }
            if(durations.get(i) > Configuration.rc.maxPhaseDuration()){
                durations.set(i, Configuration.rc.maxPhaseDuration());
            }
        }
        return repairedDurations;
    }

    /**
     * Repairs signals by
     * -> shortening the array or
     * -> restricting individual values to to the configured min or max values or
     * -> eliminating consecutive signals
     * @param signals the durations to repair
     * @return the repaired signal array
     */
    public static List<Integer> repairSignals(List<Integer> signals) {
        List<Integer> repairedSignals = signals;
        // Shorten if needed
        if(signals.size() > Configuration.rc.maxPhases()){
            repairedSignals = signals.subList(0, Configuration.rc.maxPhases());
        }
        //Set min and max, if needed
        for(int i = 0; i < signals.size(); i++){
            if(signals.get(i) < 0){
                signals.set(i, 0);
            }
            if(signals.get(i) > Configuration.sc.validSequences().size() - 1){
                signals.set(i, Configuration.sc.validSequences().size() - 1);
            }
        }
        // Eliminate consecutive signals
        int previousStateIndex = -1;
        for(int i = 0; i < repairedSignals.size(); i++){
            Integer signal = repairedSignals.get(i);
            Integer newSignal = previousStateIndex == signal ? signal + 1 >= Configuration.sc.validSequences().size() ? signal - 1 : signal + 1 : signal;
            repairedSignals.set(i, newSignal);
            previousStateIndex = newSignal;
        }
        return repairedSignals;
    }

}
