package common.service;

import DTO.Result;
import EA.EAService;
import common.data.Chromosome;
import configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

/**
 * This service randomly generates and evaluates chromosomes
 * It is used by the brute force service
 * The service is only terminated by the time limit
 */
public class RandomRunService {


    private static final Logger logger = LogManager.getLogger(RandomRunService.class.getName());

    private static String resultsFileName = "results-ea-batch-" + Configuration.getIdentifier() + "-results.csv";

    /**
     * Starts a service that creates and evaluates random chromosomes
     * @param config the configuration to use
     */
    public static void prepareAndExecuteRandom(String config){
        StatisticsService.result = new Result();
        StatisticsService.deleteFile(resultsFileName);
        StatisticsService.writeTxt(resultsFileName, "Run;Fitness;Time-VIP;Time-All\n");
        Configuration.setConfig(config);

        final ExecutorService service = Executors.newSingleThreadExecutor();
        Future future = service.submit(RandomRunService::run);
        try {
            future.get(Configuration.rc.timeLimit() - Configuration.rc.timeLimitLocalSearch(), TimeUnit.MILLISECONDS);
        } catch (final TimeoutException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            service.shutdownNow();
            ParallelisationService.abort();
        }
    }

    private static void run(){
        int i = 1;
        // Termination is done by the configured time limit through the brute force service
        while(true){
            Chromosome chromosome = new Chromosome(true);
            chromosome.evaluate(null);
            writeBest(i, chromosome);
            i++;
        }
    }

    private static void writeBest(int run, Chromosome best){
        String str = run + ";-1;-1;-1\n";
        try{
            str = run + ";" + best.getFitness().getValue() + ";" + best.getFitness().getTimeForTargetVehicle() + ";" + best.getFitness().getTimeForAllVehicles() + "\n";
        }catch (Exception e){
            e.printStackTrace();
        }
        StatisticsService.writeTxt(resultsFileName, str);
    }



}
