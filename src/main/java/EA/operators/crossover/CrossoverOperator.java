package EA.operators.crossover;

import DTO.Parents;

import java.util.List;

public interface CrossoverOperator {

    List<Integer> crossoverDurations(Parents parents);
    List<Integer> crossoverSignals(Parents parents);

}
