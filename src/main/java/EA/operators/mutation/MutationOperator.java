package EA.operators.mutation;

import java.util.List;

public interface MutationOperator {

    List<Integer> mutate(List<Integer> signals);

}
