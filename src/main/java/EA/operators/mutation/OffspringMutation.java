package EA.operators.mutation;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class OffspringMutation implements MutationOperator {

    /**
     * Mutates a list by adding an 10% offspring to n random genes
     * @param list the list to mutate
     * @return the mutated signals
     */
    @Override
    public List<Integer> mutate(List<Integer> list) {
        int numberOfGenesToModify = ThreadLocalRandom.current().nextInt(1, list.size() + 1);
        int[] randoms = ThreadLocalRandom.current().ints(0, list.size()).distinct().limit(numberOfGenesToModify).toArray();
        for(int random : randoms){
            double offspring = Math.random() < 0.5 ? 0.9 : 1.1;
            Integer newValue = list.get(random);
            if(offspring > 1.0){
                newValue = (int) Math.ceil(newValue * offspring);
            }else{
                newValue = (int) Math.floor(newValue * offspring);
            }
            list.set(random, newValue);
        }
        return list;
    }
}
