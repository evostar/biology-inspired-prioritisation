package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;
import configuration.Configuration;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RankingSelection implements SelectionOperator {

    /**
     * Selects parents by ranking selection
     * @return The parents as list of two chromosomes
     */
    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        List<Chromosome> chromosomeList = new ArrayList<>();;
        Arrays.asList(chromosomes.toArray()).forEach(c -> chromosomeList.add((Chromosome) c));
        chromosomeList.sort(Comparator.comparingDouble(c -> c.getFitness().getValue()));
        Collections.reverse(chromosomeList);

        Double scalingFactor = (Double) Configuration.ac.selectionOptions().get("scalingFactor");
        int u = chromosomes.size();

        double randomA = Math.random();
        double randomB = Math.random();
        double cumulativeProbability = 0.0;

        Chromosome parentA = null;
        Chromosome parentB = null;

        // Make sure parents are different
        boolean parentAWasJustSet = false;

        for(int i = 0; i < chromosomes.size() && (parentA == null || parentB == null); i++){
            double probability = ((2 - scalingFactor) / u) + ((2 * i * (scalingFactor - 1)) / (u * (u - 1)));
            cumulativeProbability = cumulativeProbability + probability;
            if (randomA <= cumulativeProbability && parentA == null) {
                parentA = chromosomeList.get(i);
                parentAWasJustSet = true;
            }
            if (randomB <= cumulativeProbability && !parentAWasJustSet && parentB == null) {
                parentB = chromosomeList.get(i);
            }
            parentAWasJustSet = false;
        }

        return new Parents(parentA, parentB);
    }

}
