package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Selects parents based on their species (has to be of same species)
 */
public class RestrictedBySpeciesSelection implements SelectionOperator {

    private static final Logger logger = LogManager.getLogger(RestrictedBySpeciesSelection.class.getName());

    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        TournamentSelection tournamentSelection = new TournamentSelection();
        Parents parents = tournamentSelection.select(chromosomes);
        Chromosome parentA = parents.getParentA();
        for(Chromosome chromosome : chromosomes){
            if(parentA.getSpeciesClassifier() == chromosome.getSpeciesClassifier() && !parentA.equals(chromosome)){
                parents = new Parents(parentA, chromosome);
                return parents;
            }
        }
        logger.info("Could not find a chromosome with identical species.");
        return parents;
    }

}
