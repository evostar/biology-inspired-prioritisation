package EA.operators.selection;

import DTO.Parents;
import common.data.Chromosome;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FitnessProportionateSelection implements SelectionOperator {

    /**
     * Selects parents by their fitness (the better the more likely)
     * @return The parents as list of two chromosomes
     */
    @Override
    public Parents select(ConcurrentLinkedQueue<Chromosome> chromosomes) {
        List<Chromosome> chromosomeList = new ArrayList<>();;
        Arrays.asList(chromosomes.toArray()).forEach(c -> chromosomeList.add((Chromosome) c));

        chromosomeList.sort(Comparator.comparingDouble(c -> c.getFitness().getValue()));
        Collections.reverse(chromosomeList);

        Double totalFitness = 0.0;
        for (Chromosome chromosome : chromosomeList) {
            if (!chromosome.getFitness().getWasAborted()) {
                totalFitness = totalFitness + chromosome.getFitness().getValue();
            }
        }

        double randomA = Math.random();
        double randomB = Math.random();

        Chromosome parentA = null;
        Chromosome parentB = null;

        // Make sure parents are different
        boolean parentAWasJustSet = false;

        for(int i = 0; i < chromosomeList.size() && (parentA == null || parentB == null); i++){
            double probability = 0;
            if(!chromosomeList.get(i).getFitness().getWasAborted()){
                double probabilityInverted = chromosomeList.get(i).getFitness().getValue() / totalFitness;
                probability = 1 - probabilityInverted;
            }
            if (randomA <= probability && parentA == null) {
                parentA = chromosomeList.get(i);
                parentAWasJustSet = true;
            }
            if (randomB <= probability && !parentAWasJustSet && parentB == null) {
                parentB = chromosomeList.get(i);
            }
            parentAWasJustSet = false;
        }

        if(parentA == null){
            parentA = chromosomeList.get(chromosomes.size() - 1);
        }
        if(parentB == null){
            parentB = chromosomeList.get(chromosomes.size() - 2);
        }

        return new Parents(parentA, parentB);
    }

}
