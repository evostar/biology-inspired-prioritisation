package transformers;

import common.data.Chromosome;
import configuration.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;

/**
 * Class to create signal XMLs from signals of chromosome
 * Class only uses static methods
 */
public class ChromosomeTransformer {

    /**
     * Method converts the signals of a chromosome to XML
     * See @see https://sumo.dlr.de/docs/Simulation/Traffic_Lights.html for specification
     * @param chromosome the chromosome to convert
     * @param name the name of the XML file
     * @return the path of the generated XML file
     */
    public static String transformWithName(Chromosome chromosome, String name){

        String fileName = Paths.get(".").toAbsolutePath().normalize().toString() + "/scenarios/tmp/signals/" + name;

        DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
        Document dom;

        try {
            DocumentBuilder db = icFactory.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            Element additionals = dom.createElement("additionals");

            Element tlLogic = dom.createElement("tlLogic");
            tlLogic.setAttribute("id", "0");
            tlLogic.setAttribute("type", "static");
            tlLogic.setAttribute("programID", "1");
            tlLogic.setAttribute("offset", "0");

            // For each gene (duration/signal) the corresponding phases that occur before and after the phase are also added
            Integer sourceSequenceIndex = -1;
            for(int i = 0; i < chromosome.getDurations().size(); i++){
                Integer targetSequenceIndex = chromosome.getSignals().get(i);
                List<String> sequence = Configuration.sc.validSequences().get(targetSequenceIndex);
                for(int n = 0; n < sequence.size(); n++){
                    Element phase = dom.createElement("phase");
                    List<Integer> intergreenTimes = Configuration.sc.intergreenTimes(sourceSequenceIndex + 1, targetSequenceIndex + 1);
                    String duration = intergreenTimes.get(n).toString();
                    phase.setAttribute("duration", duration.equals("-1") ? chromosome.getDurations().get(i).toString() : duration);
                    phase.setAttribute("state", sequence.get(n));
                    tlLogic.appendChild(phase);
                }
                sourceSequenceIndex = targetSequenceIndex;
            }

            additionals.appendChild(tlLogic);
            dom.appendChild(additionals);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // send DOM to file
            tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(fileName)));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return (new File(fileName)).getAbsolutePath();
    }

    public static String transformChromosome(Chromosome chromosome, String name){
        String fileName = "../signals-" + name + ".sig.xml";
        return ChromosomeTransformer.transformWithName(chromosome, fileName);
    }
    
}
