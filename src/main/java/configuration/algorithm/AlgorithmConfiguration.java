package configuration.algorithm;

import EA.diversityMechanisms.DiversityMechanism;
import EA.operators.crossover.CrossoverOperator;
import EA.operators.mutation.MutationOperator;
import EA.operators.replacement.ReplacementOperator;
import EA.operators.selection.SelectionOperator;

import java.util.Map;

/**
 * The algorithm configuration stores all parameters for the EA/PSO
 */
public interface AlgorithmConfiguration {

    public Boolean multiThreading();
    public Integer populationSize();
    public Integer numberOfChildren();
    public Integer generations();

    public boolean executeMultipleEvaluations();

    public boolean sharedFitness();
    public double sharedFitnessRadius();

    public DiversityMechanism diversityMechanism();
    // Map to store additional parameters for the chosome diversity mechanism (e.g. number of species)
    public Map<String, Object> diversityMechanismOptions();

    public SelectionOperator selectionOperator();
    public Map<String, Object> selectionOptions();

    public CrossoverOperator crossoverOperatorDuration();
    public CrossoverOperator crossoverOperatorSignals();

    public Double mutationProbability();
    public MutationOperator mutationOperatorStates();
    public MutationOperator mutationOperatorDurations();

    public ReplacementOperator replacementOperator();

}
