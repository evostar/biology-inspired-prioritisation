package configuration.scenario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractScenarioConfiguration implements ScenarioConfiguration {

    public List<List<String>> transformSequences(HashMap<String, String> states, List<List<String>> sequences){
        List<List<String>> validSequences = new ArrayList<>();
        sequences.forEach(sequence -> {
            List<String> validSequence = new ArrayList<>();
            sequence.forEach(state -> {
                validSequence.add(states.get(state));
            });
            validSequences.add(validSequence);
        });
        return  validSequences;
    }
    
}
