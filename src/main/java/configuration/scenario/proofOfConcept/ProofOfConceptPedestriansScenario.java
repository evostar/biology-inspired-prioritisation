package configuration.scenario.proofOfConcept;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ProofOfConceptPedestriansScenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(ProofOfConceptPedestriansScenario.class.getName());

    @Override
    public String network() {
        return "poc-pedestrian/network.net.xml";
    }

    @Override
    public String routes() {
        return "poc-pedestrian/routes.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "poc-pedestrian/signals-default.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrrrrrrrrrr");

        states.put("EAST_WEST_YELLOW_GREEN", "rrrryyyyyrrrryyyyyrrrr");
        states.put("EAST_WEST_GREEN", "rrrrgGGGgrrrrgGGGgGrGr");
        states.put("EAST_WEST_GREEN_NO_PED", "rrrrgGGGgrrrrgGGGgrrrr");
        states.put("EAST_WEST_YELLOW_RED", "rrrryyyyyyrrryyyyyrrrr");

        states.put("EAST_WEST_LEFT_YELLOW_GREEN", "rrrryrryrrrrryrryrrrrr");
        states.put("EAST_WEST_LEFT_GREEN", "rrrrGrrrGrrrrGrrrGrrrr");
        states.put("EAST_WEST_LEFT_GREEN_NO_PED", "rrrrGrrrGrrrrGrrrGrrrr");
        states.put("EAST_WEST_LEFT_YELLOW_RED", "rrrryrrryrrrryrrryrrrr");

        states.put("NORTH_SOUTH_YELLOW_GREEN", "yyyyrrrrryyyyrrrrrrrrr");
        states.put("NORTH_SOUTH_GREEN", "gGGgrrrrrgGGgrrrrrrGrG");
        states.put("NORTH_SOUTH_GREEN_NO_PED", "gGGgrrrrrgGGgrrrrrrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyyrrrrryyyyrrrrrrrrr");

        states.put("NORTH_SOUTH_LEFT_YELLOW_GREEN", "yrryrrrrryrryrrrrrrrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN", "GrrGrrrrrGrrGrrrrrrrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN_NO_PED", "GrrGrrrrrGrrGrrrrrrrrr");
        states.put("NORTH_SOUTH_LEFT_YELLOW_RED", "yrryrrrrryrryrrrrrrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceA = Arrays.asList("EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_GREEN_NO_PED", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceB = Arrays.asList("EAST_WEST_LEFT_YELLOW_GREEN", "EAST_WEST_LEFT_GREEN", "EAST_WEST_LEFT_GREEN_NO_PED", "EAST_WEST_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceC = Arrays.asList("NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_GREEN_NO_PED", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceD = Arrays.asList("NORTH_SOUTH_LEFT_YELLOW_GREEN", "NORTH_SOUTH_LEFT_GREEN", "NORTH_SOUTH_LEFT_GREEN_NO_PED", "NORTH_SOUTH_LEFT_YELLOW_RED", "ALL_RED");

        List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceB, validSequenceC, validSequenceD);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            case 0:
                return Arrays.asList(1, -1, 5, 1, 10);
            case 1:
            case 3:
                switch (targetState){
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 5, 1, 10);
                }
            case 2:
            case 4:
                switch (targetState){
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 10, 1, 10);
                }
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 500;
    }
}
