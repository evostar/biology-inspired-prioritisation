package configuration.scenario.proofOfConcept;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ProofOfConceptScenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(ProofOfConceptScenario.class.getName());

    @Override
    public String network() {
        return "poc/network.net.xml";
    }

    @Override
    public String routes() {
        return "poc/routes.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "poc/signals-default.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrrrrrr");

        states.put("EAST_WEST_YELLOW_GREEN", "rrrryyyyyrrrryyyyy");
        states.put("EAST_WEST_GREEN", "rrrrgGGgGrrrrgGGgG");
        states.put("EAST_WEST_YELLOW_RED", "rrrryyyyyyrrryyyyy");

        states.put("EAST_WEST_LEFT_YELLOW_GREEN", "rrrryrryrrrrryrryr");
        states.put("EAST_WEST_LEFT_GREEN", "rrrrGrrGrrrrrGrrGr");
        states.put("EAST_WEST_LEFT_YELLOW_RED", "rrrryrryrrrrryrryr");

        states.put("NORTH_SOUTH_YELLOW_GREEN", "yyyyrrrrryyyyrrrrr");
        states.put("NORTH_SOUTH_GREEN", "gGGgrrrrrgGGgrrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyyrrrrryyyyrrrrr");

        states.put("NORTH_SOUTH_LEFT_YELLOW_GREEN", "yrryrrrrryrryrrrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN", "GrrGrrrrrGrrGrrrrr");
        states.put("NORTH_SOUTH_LEFT_YELLOW_RED", "yrryrrrrryrryrrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceA = Arrays.asList("EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceB = Arrays.asList("EAST_WEST_LEFT_YELLOW_GREEN", "EAST_WEST_LEFT_GREEN", "EAST_WEST_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceC = Arrays.asList("NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceD = Arrays.asList("NORTH_SOUTH_LEFT_YELLOW_GREEN", "NORTH_SOUTH_LEFT_GREEN", "NORTH_SOUTH_LEFT_YELLOW_RED", "ALL_RED");

        List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceB, validSequenceC, validSequenceD);
        //List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceB, validSequenceC);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                switch (targetState){
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 1, 10);
                }
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 500;
    }
}
