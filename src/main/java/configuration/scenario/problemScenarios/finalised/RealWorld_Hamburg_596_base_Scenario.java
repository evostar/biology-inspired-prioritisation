package configuration.scenario.problemScenarios.finalised;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RealWorld_Hamburg_596_base_Scenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(RealWorld_Hamburg_596_base_Scenario.class.getName());

    @Override
    public String network() {
        return "596-finalised/network.net.xml";
    }

    @Override
    public String routes() {
        return "596-finalised/base.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "596-finalised/signals-default-adaptive.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrr");

        states.put("EAST_WEST_YELLOW_GREEN", "rrruuuurrruuuu");
        states.put("EAST_WEST_GREEN", "rrrgGGgrrrgGGg");
        states.put("EAST_WEST_YELLOW_RED", "rrryyyyrrryyyy");

        states.put("EAST_WEST_LEFT_YELLOW_GREEN", "rrrurrurrrurru");
        states.put("EAST_WEST_LEFT_GREEN", "rrrGrrGrrrGrrG");
        states.put("EAST_WEST_LEFT_YELLOW_RED", "rrryrryrrryrry");

        states.put("NORTH_SOUTH_YELLOW_GREEN", "uuurrrruuurrrr");
        states.put("NORTH_SOUTH_GREEN", "gGgrrrrgGgrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyrrrryyyrrrr");

        states.put("NORTH_SOUTH_LEFT_YELLOW_GREEN", "ururrrrururrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN", "GrGrrrrGrGrrrr");
        states.put("NORTH_SOUTH_LEFT_YELLOW_RED", "yryrrrryryrrrr");

        states.put("EAST_YELLOW_GREEN", "rrruuuurrrrrrr");
        states.put("EAST_GREEN", "rrrGGGGrrrrrrr");
        states.put("EAST_YELLOW_RED", "rrryyyyrrrrrrr");

        states.put("WEST_YELLOW_GREEN", "rrrrrrrrrruuuu");
        states.put("WEST_GREEN", "rrrrrrrrrrGGGG");
        states.put("WEST_YELLOW_RED", "rrrrrrrrrryyyy");

        states.put("NORTH_YELLOW_GREEN", "uuurrrrrrrrrrr");
        states.put("NORTH_GREEN", "GGGrrrrrrrrrrr");
        states.put("NORTH_YELLOW_RED", "yyyrrrrrrrrrrr");

        states.put("SOUTH_YELLOW_GREEN", "rrrrrrruuurrrr");
        states.put("SOUTH_GREEN", "rrrrrrrGGGrrrr");
        states.put("SOUTH_YELLOW_RED", "rrrrrrryyyrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceEW = Arrays.asList("EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceEWL = Arrays.asList("EAST_WEST_LEFT_YELLOW_GREEN", "EAST_WEST_LEFT_GREEN", "EAST_WEST_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceNS = Arrays.asList("NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceNSL = Arrays.asList("NORTH_SOUTH_LEFT_YELLOW_GREEN", "NORTH_SOUTH_LEFT_GREEN", "NORTH_SOUTH_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceE = Arrays.asList("EAST_YELLOW_GREEN", "EAST_GREEN", "EAST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceW = Arrays.asList("WEST_YELLOW_GREEN", "WEST_GREEN", "WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceN = Arrays.asList("NORTH_YELLOW_GREEN", "NORTH_GREEN", "NORTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceS = Arrays.asList("SOUTH_YELLOW_GREEN", "SOUTH_GREEN", "SOUTH_YELLOW_RED", "ALL_RED");

        //List<List<String>> sequences = Arrays.asList(validSequenceEW, validSequenceEWL, validSequenceNS, validSequenceNSL);
        //List<List<String>> sequences = Arrays.asList(validSequenceEW, validSequenceNS);
        List<List<String>> sequences = Arrays.asList(validSequenceEW, validSequenceNS, validSequenceE, validSequenceW, validSequenceN, validSequenceS);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            // N/S to E/W
            case 2:
            case 5:
            case 6:
                return Arrays.asList(1, -1, 3, 3);
            // E/W to N/S + start
            case 1:
            case 3:
            case 4:
            case 0:
                return Arrays.asList(1, -1, 3, 5);
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 1000;
    }
}
