package configuration.scenario.problemScenarios.revised;

import configuration.scenario.AbstractScenarioConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ProblemScenario_IV_Scenario extends AbstractScenarioConfiguration {

    private static final Logger logger = LogManager.getLogger(ProblemScenario_IV_Scenario.class.getName());

    @Override
    public String network() {
        return "596-morning/network.net.xml";
    }

    @Override
    public String routes() {
        return "596-morning/PS-II-II.rou.xml";
    }

    @Override
    public String defaultSignals() {
        return "596-morning/signals-default-adaptive.sig.xml";
    }

    @Override
    public String idOfTargetVehicle() {
        return "VIP";
    }

    @Override
    public HashMap<String, String> states(){
        HashMap<String, String> states = new HashMap<>();

        states.put("ALL_RED", "rrrrrrrrrrrrrr");

        states.put("EAST_WEST_YELLOW_GREEN", "rrruuuurrruuuu");
        states.put("EAST_WEST_GREEN", "rrrgGGgrrrgGGg");
        states.put("EAST_WEST_YELLOW_RED", "rrryyyyrrryyyy");

        states.put("EAST_WEST_LEFT_YELLOW_GREEN", "rrrurrurrrurru");
        states.put("EAST_WEST_LEFT_GREEN", "rrrGrrGrrrGrrG");
        states.put("EAST_WEST_LEFT_YELLOW_RED", "rrryrryrrryrry");

        states.put("NORTH_SOUTH_YELLOW_GREEN", "uuurrrruuurrrr");
        states.put("NORTH_SOUTH_GREEN", "gGgrrrrgGgrrrr");
        states.put("NORTH_SOUTH_YELLOW_RED", "yyyrrrryyyrrrr");

        states.put("NORTH_SOUTH_LEFT_YELLOW_GREEN", "ururrrrururrrr");
        states.put("NORTH_SOUTH_LEFT_GREEN", "GrGrrrrGrGrrrr");
        states.put("NORTH_SOUTH_LEFT_YELLOW_RED", "yryrrrryryrrrr");

        return states;
    }

    @Override
    public List<List<String>> validSequences() {
        HashMap<String, String> states = states();
        List<String> validSequenceA = Arrays.asList("EAST_WEST_YELLOW_GREEN", "EAST_WEST_GREEN", "EAST_WEST_YELLOW_RED", "ALL_RED");
        List<String> validSequenceB = Arrays.asList("EAST_WEST_LEFT_YELLOW_GREEN", "EAST_WEST_LEFT_GREEN", "EAST_WEST_LEFT_YELLOW_RED", "ALL_RED");
        List<String> validSequenceC = Arrays.asList("NORTH_SOUTH_YELLOW_GREEN", "NORTH_SOUTH_GREEN", "NORTH_SOUTH_YELLOW_RED", "ALL_RED");
        List<String> validSequenceD = Arrays.asList("NORTH_SOUTH_LEFT_YELLOW_GREEN", "NORTH_SOUTH_LEFT_GREEN", "NORTH_SOUTH_LEFT_YELLOW_RED", "ALL_RED");

        //List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceB, validSequenceC, validSequenceD);
        List<List<String>> sequences = Arrays.asList(validSequenceA, validSequenceC);
        return transformSequences(states, sequences);
    }

    @Override
    public List<Integer> intergreenTimes(Integer sourceState, Integer targetState){
        switch (sourceState){
            case 1:
            case 2:
                switch (targetState){
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 3);
                }
            case 3:
            case 4:
                switch (targetState){
                    case 1:
                    case 2:
                        return Arrays.asList(1, -1, 3, 5);
                }
            case 0:
                switch (targetState){
                    case 1:
                    case 2:
                        return Arrays.asList(1, -1, 3, 5);
                    case 3:
                    case 4:
                        return Arrays.asList(1, -1, 3, 3);
                }
        }

        logger.error("No valid intergreen times for: " + sourceState + " - " + targetState);
        return null;
    }

    @Override
    public Integer maxSimulationTime() {
        return 1000;
    }
}
