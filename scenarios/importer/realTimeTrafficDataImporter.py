'''
 -> Script to convert real time traffic data to SUMO route file
 -> The script produces trips
'''

# %%
import numpy as np
import pandas as pd
import sys
import json
import random

# %%
arguments = sys.argv or []

if 'ipykernel_launcher' in str(arguments[0]):
  arguments = []

def getArg(i, default):
  n = i + 1
  if(n > len(arguments) - 1):
    return default
  return arguments[n]

# %%
real_time_traffic_data = pd.read_csv(getArg(0, './596/realTimeTrafficData.csv'), sep=';')
with open(getArg(1, './596/layout.json')) as json_file:
    layout = json.load(json_file)

# %%
header = '''\
<?xml version="1.0" encoding="UTF-8"?>

<routes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/routes_file.xsd">
  '''

vehicle_types = '''\
    <!-- Vehicle Types -->

    <vType id="bus" vClass="bus"/>
    <vType id="taxi" vClass="taxi"/>
    <vType id="passenger" vClass="passenger"/>
    <vType id="truck" vClass="truck"/>
    <vType id="motorcycle" vClass="motorcycle"/>
    <vType id="bicycle" vClass="bicycle"/>

    <vType id="pedestrian" vClass="pedestrian"/>

    <!-- Vehicle Types -->
    '''

content = ''

'''\
  <vehicle id="0" depart="0.00" departPos="base" departSpeed="0" departLane="random" type="bicycle">
        <route edges="EastTop NorthRight" color="yellow"/>
      </vehicle>
  '''

footer = '''\
</routes>
  '''

#%%
vehicles = []

edges = layout['edges']

for index,row in real_time_traffic_data.iterrows():

  # basic info
  vehicleId = str(index)
  departSpeed = 'max' #layout['avgSpeed']
  color = 'yellow'
  randomVehicleType = random.randrange(0, 10 + 1, 1)
  vehicleType = 'passenger'
  if randomVehicleType >= 9:
    vehicleType = 'truck'
    # departSpeed = 30

  # start Pos
  startEdge = int(row['Edge'])
  departLane = int(row['Lane']) - 1
  departPos = 0

  # destination
  for edge in edges:
    if edge['id'] == startEdge:
      i = 0
      for laneObj in edge['lanes']:
        if departLane == i:
          endEdge = random.choice (laneObj['destinations'])
        i = i + 1

  # depart Time
  maxDepartTime = int(row['Time'])
  minDepartTime = max(0, maxDepartTime - 5)
  departTime = random.randint(minDepartTime, maxDepartTime)
  
  for i in range(0, int(row['Vehicles'])):
    vehicles.append(
      [
        departTime,
        '<vehicle id="' + vehicleId + '-' + str(i) + '" depart="' + str(departTime) + '" departPos="' + str(departPos) + '" departSpeed="' + str(departSpeed) + '" departLane="' + str(departLane) + '" type="' + vehicleType + '"><route edges="' + str(startEdge) + ' ' + str(endEdge) + '" color="' + color + '"/></vehicle>\n'
      ]
    )

# %%
content = ""

for vehicle in sorted(vehicles):
  content = content + vehicle[1]

# %%
f = open(getArg(1, './596/routes.rou.xml'), "w")
f.write(header + vehicle_types + content + footer)
f.close()

# %%
